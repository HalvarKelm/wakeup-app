package com.halvar.wakeup;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.AlarmClock;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class CreateAlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("WakeUp", "CREATING ALARM");

        // Get the current time
        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(System.currentTimeMillis());
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);

        // Create a new intent which sets an alarm in 1 min
        Intent alarm = new Intent(AlarmClock.ACTION_SET_ALARM);
        alarm.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        alarm.putExtra(AlarmClock.EXTRA_MESSAGE, "WakeUp");
        alarm.putExtra(AlarmClock.EXTRA_HOUR, hour);
        alarm.putExtra(AlarmClock.EXTRA_MINUTES, minute+1);
        alarm.putExtra(AlarmClock.EXTRA_SKIP_UI, true);

        // Make sure the App is allowed to be displayed over other Apps otherwise this will fail
        context.startActivity(alarm);
    }
}