package com.halvar.wakeup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.drawable.IconCompat;
import androidx.fragment.app.DialogFragment;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.StatusBarManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

public class MainActivity extends AppCompatActivity {

    public final int standardSleepH = 8;
    public final int standardSleepM = 0;
    public final int standardEarlyH = 6;
    public final int standardEarlyM = 0;
    public final int standardLateH = 12;
    public final int standardLateM = 0;

    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createNotificationChannel();

        // Elements
        CheckBox enabled = findViewById(R.id.enabledCheckbox);
        CheckBox notification = findViewById(R.id.notificationCheckbox);
        CheckBox waketime = findViewById(R.id.waketimeCheckbox);
        Button sleepButton = findViewById(R.id.sleepButton);
        Button earlyButton = findViewById(R.id.earlyButton);
        Button lateButton = findViewById(R.id.lateButton);

        // Get Shared Preferences
        SharedPreferences sharedPreferences = getSharedPreferences("WakeUp", Context.MODE_PRIVATE);
        Log.d("WakeUp", sharedPreferences.contains("sleepH")+" " +sharedPreferences.contains("sleepM"));

        // If hours or minutes have not been set, set them as the default values
        if(!sharedPreferences.contains("sleepH") || !sharedPreferences.contains("sleepM")) {
            SharedPreferences.Editor editor = sharedPreferences.edit();

            // Standard sleep time
            editor.putInt("sleepH", standardSleepH);
            editor.putInt("sleepM", standardSleepM);

            // Standard early time
            editor.putInt("earlyH", standardEarlyH);
            editor.putInt("earlyM", standardEarlyM);

            // Standard late time
            editor.putInt("lateH", standardLateH);
            editor.putInt("lateM", standardLateM);

            editor.apply();

            // Also show Request to add Tile
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                StatusBarManager statusBar = this.getSystemService(StatusBarManager.class);
                ComponentName componentName = new ComponentName(this.getApplicationContext(), WakeUpTile.class);
                statusBar.requestAddTileService(
                        componentName,
                        "WakeUp",
                        IconCompat.createWithResource(this.getApplicationContext(), R.drawable.ic_launcher_foreground).toIcon(this),
                        getMainExecutor(),
                        (none) -> {
                        });
            }
        }

        // get the hours and minutes from the shared preferences and set the button
        int hours = sharedPreferences.getInt("sleepH", standardSleepH);
        int minutes = sharedPreferences.getInt("sleepM", standardSleepM);
        sleepButton.setText("Sleep for "+String.format("%02d", hours)+":"+String.format("%02d", minutes));
        int earlyHours = sharedPreferences.getInt("earlyH", standardEarlyH);
        int earlyMinutes = sharedPreferences.getInt("earlyM", standardEarlyM);
        earlyButton.setText("From "+String.format("%02d", earlyHours)+":"+String.format("%02d", earlyMinutes));
        int lateHours = sharedPreferences.getInt("laetH", standardLateH);
        int lateMinutes = sharedPreferences.getInt("lateM", standardLateM);
        lateButton.setText("To "+String.format("%02d", lateHours)+":"+String.format("%02d", lateMinutes));


        // Listen to enabled checkbox
        enabled.setChecked(sharedPreferences.getBoolean("enabled", false));
        enabled.setOnCheckedChangeListener((buttonView, isChecked) -> {
            Log.d("WakeUp", "Auto Alarm "+isChecked);

            // Set the checked value
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("enabled", isChecked);

            editor.apply();
        });
        // Listen to notification checkbox
        notification.setChecked(sharedPreferences.getBoolean("notification", false));
        notification.setOnCheckedChangeListener((buttonView, isChecked) -> {
            Log.d("WakeUp", "Notification "+isChecked);

            // Set the checked value
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("notification", isChecked);

            editor.apply();
        });
        // Listen to waketime checkbox
        waketime.setChecked(sharedPreferences.getBoolean("waketime", false));
        waketime.setOnCheckedChangeListener((buttonView, isChecked) -> {
            Log.d("WakeUp", "Waketime "+isChecked);

            // Set the checked value
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("waketime", isChecked);

            editor.apply();
        });


        // Initialize the Screen Receiver Service
        Intent intent = new Intent(this, ScreenService.class);
        startService(intent);
    }

    public void showSleepTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment("sleep");
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    public void showEarlyTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment("early");
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    public void showLateTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment("late");
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    public void openSettings(View v){
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
        startActivity(intent);
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "WakeUp";
            String description = "WakeUp";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("WakeUp", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            channel.setVibrationPattern(new long[] { 0, 30 });
            notificationManager.createNotificationChannel(channel);
        }
    }

}