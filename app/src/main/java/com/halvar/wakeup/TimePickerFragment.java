package com.halvar.wakeup;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TimePicker;

import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

public class TimePickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    String name = "";

    public TimePickerFragment(String name) {
        this.name = name;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();

        // Get the current hours and minutes
        SharedPreferences sharedPreferences = requireActivity().getSharedPreferences("WakeUp", Context.MODE_PRIVATE);
        int hours = sharedPreferences.getInt(this.name+"H", 8);
        int minutes = sharedPreferences.getInt(this.name+"M", 0);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hours, minutes, true);
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    public void onTimeSet(TimePicker view, int hours, int minutes) {
        // Do something with the time chosen by the user
        Log.d("WakeUp", "Set "+this.name+" to " + hours + ":"+ minutes + " ");

        // Set the new hours and minutes
        SharedPreferences sharedPreferences = requireActivity().getSharedPreferences("WakeUp", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(this.name+"H", hours);
        editor.putInt(this.name+"M", minutes);
        editor.apply();

        // Update the UI
        if(this.name.equals("sleep")){
            Button sleepButton = (Button) requireActivity().findViewById(R.id.sleepButton);
            sleepButton.setText("Sleep for "+String.format("%02d", hours)+":"+String.format("%02d", minutes));
        } else if (this.name.equals("early")) {
            Button earlyButton = (Button) requireActivity().findViewById(R.id.earlyButton);
            earlyButton.setText("From "+String.format("%02d", hours)+":"+String.format("%02d", minutes));
        } else if (this.name.equals("late")) {
            Button lateButton = (Button) requireActivity().findViewById(R.id.lateButton);
            lateButton.setText("To "+String.format("%02d", hours)+":"+String.format("%02d", minutes));
        }
    }
}
