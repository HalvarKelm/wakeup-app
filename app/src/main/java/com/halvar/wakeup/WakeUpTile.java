package com.halvar.wakeup;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.service.quicksettings.Tile;
import android.service.quicksettings.TileService;

import androidx.core.graphics.drawable.IconCompat;

public class WakeUpTile extends TileService {

    // Called when the user adds your tile.
    @Override
    public void onTileAdded() {
        super.onTileAdded();
    }

    // Called when your app can update your tile.
    @SuppressLint("DefaultLocale")
    @Override
    public void onStartListening() {
        super.onStartListening();

        // Get the current hours and minutes
        SharedPreferences sharedPreferences = getSharedPreferences("WakeUp", Context.MODE_PRIVATE);
        boolean enabled = sharedPreferences.getBoolean("enabled", false);
        int hours = sharedPreferences.getInt("sleepH", -1);
        int minutes = sharedPreferences.getInt("sleepM", -1);

        // Set the Tile info
        Tile tile = getQsTile();
        if(hours >= 0 && minutes >= 0 && Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
            tile.setSubtitle("After "+ String.format("%02d", hours)+":"+String.format("%02d", minutes));
        }
        tile.setLabel("WakeUp");
        tile.setState(enabled ? Tile.STATE_ACTIVE : Tile.STATE_INACTIVE);
        tile.updateTile();

        // Initialize the Screen Receiver Service
        Intent intent = new Intent(this, ScreenService.class);
        startService(intent);
    }


    // Called when your app can no longer update your tile.
    @Override
    public void onStopListening() {
        super.onStopListening();
    }

    // Called when the user taps on your tile in an active or inactive state.
    @Override
    public void onClick() {
        super.onClick();
        Tile tile = getQsTile();

        // Get the current hours and minutes
        SharedPreferences sharedPreferences = getSharedPreferences("WakeUp", Context.MODE_PRIVATE);
        boolean newEnabled = !sharedPreferences.getBoolean("enabled", false);

        // Toggle whether the auto alarm is enabled or not
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("enabled", newEnabled);

        // Apply the change
        if(sharedPreferences.contains("sleepH")  && sharedPreferences.contains("sleepM")) {
            editor.apply();
            tile.setState(newEnabled ? Tile.STATE_ACTIVE : Tile.STATE_INACTIVE);
        }

        // Update the tile
        tile.updateTile();

        // Initialize the Screen Receiver Service
        Intent intent = new Intent(this, ScreenService.class);
        startService(intent);
    }

    // Called when the user removes your tile.
    @Override
    public void onTileRemoved() {
        super.onTileRemoved();
    }
}
