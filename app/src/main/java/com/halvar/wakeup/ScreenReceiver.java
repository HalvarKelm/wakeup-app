package com.halvar.wakeup;


import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.util.Calendar;
import java.util.Date;

public class ScreenReceiver extends BroadcastReceiver {

    @RequiresApi(api = Build.VERSION_CODES.S)
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("WakeUp", "Screen state changed");

        // Setup the managers
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        // Setup the intents
        Intent alarm = new Intent(context, CreateAlarmReceiver.class);
        alarm.setAction(Settings.ACTION_REQUEST_SCHEDULE_EXACT_ALARM);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 1001, alarm, PendingIntent.FLAG_IMMUTABLE);

        // Remove the alarm if the screen has been turned on
        if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            Log.d("WakeUp", "SCREEN ON - Alarm removed");
            if (alarmMgr!= null) {
                alarmMgr.cancel(alarmIntent);
            }
            // Cancel the notification if it exists
            notificationManager.cancel(1001002);
            return;
        }

        // Get amount of sleep
        SharedPreferences sharedPreferences = context.getSharedPreferences("WakeUp", Context.MODE_PRIVATE);

        // No preferences set
        if(!sharedPreferences.contains("enabled") ||!sharedPreferences.contains("sleepH") || !sharedPreferences.contains("sleepM")){
            Log.d("WakeUp", "No preferences set");
            return;
        }

        // Not enabled
        if(sharedPreferences.contains("enabled") && !sharedPreferences.getBoolean("enabled", false)){
            Log.d("WakeUp", "Not enabled");
            return;
        }

        // Get the hours and minutes of sleep
        int hours = sharedPreferences.getInt("sleepH", 8);
        int minutes = sharedPreferences.getInt("sleepM", 0);
        boolean waketime = sharedPreferences.getBoolean("waketime", false);
        int earlyH = sharedPreferences.getInt("earlyH", 8);
        int earlyM = sharedPreferences.getInt("earlyM", 0);
        int lateH = sharedPreferences.getInt("lateH", 8);
        int lateM = sharedPreferences.getInt("lateM", 0);

        // Not in waketime
        if(waketime && !isInWakeTime(hours, minutes, earlyH, earlyM, lateH, lateM)){
            return;
        }

        // Screen is off, set the alarm
        if (intent.getAction() != null && intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            Log.d("WakeUp", "SCREEN OFF - Alarm set in " + hours +":" +minutes);

            // Set the alarm in the manager
            alarmMgr.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime()
                    // Add hours to wait
                    + hours * 3600000L
                    // Add minutes to wait
                    + minutes * 60000L
                    // Remove 1 min since the alarm takes 1 min to set
                    - 60 * 1000, alarmIntent);

            // In case the user wants to get a small vibration, start the notification
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O && (sharedPreferences.getBoolean("notification", false))) {
                @SuppressLint("DefaultLocale") NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "WakeUp")
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setContentTitle("WakeUp")
                        //.setContentText("After "+String.format("%02d", hours)+":"+String.format("%02d", minutes))
                        .setVibrate(new long[]{0, 30})
                        .setTimeoutAfter(2000)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                notificationManager.notify(1001002, builder.build());
            }
        }
    }

    public boolean isInWakeTime(int hours, int minutes, int earlyH, int earlyM, int lateH, int lateM){
        Calendar alarm = Calendar.getInstance();
        alarm.add(Calendar.HOUR_OF_DAY, hours);
        alarm.add(Calendar.MINUTE, minutes);

        Calendar early = (Calendar) alarm.clone();
        early.set(Calendar.HOUR_OF_DAY, earlyH);
        early.set(Calendar.MINUTE, earlyM);

        Calendar late = (Calendar) alarm.clone();
        late.set(Calendar.HOUR_OF_DAY, lateH);
        late.set(Calendar.MINUTE, lateM);

        return alarm.after(early) && alarm.before(late);
    }
}